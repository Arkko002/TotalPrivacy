#define WIN32_LEAN_AND_MEAN

#include <winsock2.h>
#include <windows.h>
#include <iphlpapi.h>
#include <stdlib.h>
#include <stdio.h>
#include <wchar.h>
#include <scrnsave.h>

#define IDC_BUTTON 100
#define IDC_EDIT 101
#define IDC_COMBOBOX 102

#define MAX_KEY_LENGTH 255
#define MAX_VALUE_NAME 16383

char g_m_ClassName[] = "MAC spoofer";

void Spoof_MAC_Addr(PIP_ADAPTER_INFO selected_adapter, char* new_mac)
{
    HKEY root_adapter_key;
    TCHAR subkey_name[MAX_KEY_LENGTH];
    DWORD cb_name;
    TCHAR ach_class[MAX_PATH] = TEXT("");
    DWORD cch_class_name = MAX_PATH;
    DWORD c_sub_keys = 0;
    DWORD cb_max_sub_key;
    DWORD cch_max_class;
    DWORD c_values;
    DWORD cch_max_value;
    DWORD cb_max_value_data;
    DWORD cb_security_descriptor;
    FILETIME ft_last_write_time;

    DWORD subkey_index, value_index;
    LONG ret_code;

    ret_code = RegOpenKeyEx(HKEY_LOCAL_MACHINE,
                           TEXT("SYSTEM\\CurrentControlSet\\Control\\Class\\{4D36E972-E325-11CE-BFC1-08002BE10318}"),
                           0, KEY_ALL_ACCESS, &root_adapter_key);

    if (ret_code != ERROR_SUCCESS)
    {
        printf("Error while opening root key");
        return;
    }

    ret_code = RegQueryInfoKey(
            root_adapter_key,
            ach_class,
            &cch_class_name,
            NULL,
            &c_sub_keys,
            &cb_max_sub_key,
            &cch_max_class,
            &c_values,
            &cch_max_value,
            &cb_max_value_data,
            &cb_security_descriptor,
            &ft_last_write_time
            );

    if (ret_code != ERROR_SUCCESS)
    {
        printf("Error while querring reg. keys");
        return;
    }

    for (subkey_index = 0; subkey_index < c_sub_keys; subkey_index++)
    {
        HKEY sub_adapter_key;
        char key_dir[2056] = TEXT("SYSTEM\\CurrentControlSet\\Control\\Class\\{4D36E972-E325-11CE-BFC1-08002BE10318}\\");

        cb_name = MAX_KEY_LENGTH;
        ret_code = RegEnumKeyEx(root_adapter_key, subkey_index,
                                subkey_name, &cb_name, NULL, NULL, NULL, &ft_last_write_time);

        if (ret_code != ERROR_SUCCESS)
        {
            printf("Error while enumerating keys");
            return;
        }

        strcat(key_dir, subkey_name);

        ret_code = RegOpenKeyEx(HKEY_LOCAL_MACHINE, key_dir, 0, KEY_ALL_ACCESS, &sub_adapter_key);

        if (ret_code != ERROR_SUCCESS)
        {
            printf("Error while opening subkey");
            return;
        }

        DWORD sub_cb_name;
        TCHAR sub_ach_class[MAX_PATH] = TEXT("");
        DWORD sub_cch_class_name = MAX_PATH;
        DWORD sub_c_sub_keys = 0;
        DWORD sub_cb_max_sub_key;
        DWORD sub_cch_max_class;
        DWORD sub_c_values;
        DWORD sub_cch_max_value;
        DWORD sub_cb_max_value_data;
        DWORD sub_cb_security_descriptor;
        FILETIME sub_ft_last_write_time;

        ret_code = RegQueryInfoKey(
                sub_adapter_key,
                sub_ach_class,
                &sub_cch_class_name,
                NULL,
                &sub_c_sub_keys,
                &sub_cb_max_sub_key,
                &sub_cch_max_class,
                &sub_c_values,
                &sub_cch_max_value,
                &sub_cb_max_value_data,
                &sub_cb_security_descriptor,
                &sub_ft_last_write_time
        );

        if(ret_code != ERROR_SUCCESS)
        {
            printf("Error while quering subkey info");
            return;
        }

        char lp_value_name[sub_cch_max_value + 2];
        DWORD size_name = sub_cch_max_value + 2;
        char lp_value_data[sub_cb_max_value_data + 2];
        DWORD size_data = sub_cb_max_value_data + 2;

        for (value_index = 0; value_index < sub_c_values; ++value_index)
        {
            ret_code = RegEnumValueA(sub_adapter_key, value_index, lp_value_name,
                                     &size_name, NULL, NULL, (LPBYTE)lp_value_data, &size_data);

            size_name = sub_cch_max_value + 2;
            size_data = sub_cb_max_value_data + 2;

            if (ret_code != ERROR_SUCCESS)
            {
                printf("Error while enumerating values");
                return;
            }

            if (strstr(lp_value_name, "DriverDesc") != NULL &&
                strstr(lp_value_data, selected_adapter->Description) != NULL)
            {
                ret_code = RegSetValueEx(sub_adapter_key, "NetworkAddress", 0, REG_SZ,
                                        (const BYTE*)new_mac, sizeof(new_mac));

                if (ret_code != ERROR_SUCCESS)
                {
                    printf("Error while setting the value");
                    return;
                }

                return;
            }
        }
    }
}

IP_ADAPTER_INFO* Get_Adp_Info()
{
    IP_ADAPTER_INFO *p_adapter_info;
    ULONG ul_out_buf_len;

    p_adapter_info = (IP_ADAPTER_INFO*) malloc(sizeof(IP_ADAPTER_INFO));
    ul_out_buf_len = sizeof(IP_ADAPTER_INFO);

    if (GetAdaptersInfo(p_adapter_info, &ul_out_buf_len) != ERROR_SUCCESS)
    {
        free(p_adapter_info);
        p_adapter_info = (IP_ADAPTER_INFO*) malloc(ul_out_buf_len);
    }

    if (GetAdaptersInfo(p_adapter_info, &ul_out_buf_len) != ERROR_SUCCESS)
    {
        MessageBox(NULL, "Error while geting adapters info failed", "Error", MB_ICONEXCLAMATION | MB_OK);
    }

    return p_adapter_info;
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT msg, WPARAM w_param, LPARAM l_param)
{
    switch (msg)
    {
        case WM_CREATE:
            CreateWindowEx(WS_EX_STATICEDGE, "BUTTON", "Spoof",
                    WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_DEFPUSHBUTTON,
                    300, 10, 80, 20, hwnd, IDC_BUTTON,
                    (HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE),
                    NULL);

            CreateWindowEx(WS_EX_STATICEDGE, "EDIT", NULL,
                    WS_TABSTOP | WS_VISIBLE | WS_CHILD | WS_BORDER,
                    10, 10, 270, 20, hwnd, IDC_EDIT,
                    (HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE),
                    NULL);

            CreateWindowEx(WS_EX_STATICEDGE, "COMBOBOX", NULL,
            CBS_DROPDOWN | CBS_HASSTRINGS | WS_VISIBLE | WS_CHILD | WS_BORDER | WS_OVERLAPPED,
            10, 40, 270, 20, hwnd, IDC_COMBOBOX,
            (HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE),
            NULL);
            break;

        case WM_COMMAND:
            if (LOWORD(w_param) == IDC_BUTTON)
            {
                HWND text_edit = GetDlgItem(hwnd, IDC_EDIT);
                TCHAR new_mac[16];
                GetWindowTextA(text_edit, new_mac, 16);

                HWND combobox = GetDlgItem(hwnd, IDC_COMBOBOX);
                int item_index = SendMessage(combobox, CB_GETCURSEL, 0, 0);
                TCHAR selected_adapter[256];
                SendMessage(combobox, CB_GETLBTEXT, (WPARAM)item_index, (LPARAM)selected_adapter);

                PIP_ADAPTER_INFO p_adater = Get_Adp_Info();
                while (p_adater)
                {
                    if (strstr(p_adater->Description, selected_adapter) != NULL)
                    {
                        Spoof_MAC_Addr(p_adater, new_mac);
                    }

                    p_adater = p_adater->Next;
                }
            }
            break;

        case WM_CLOSE:
            DestroyWindow(hwnd);
            break;

        case WM_DESTROY:
            PostQuitMessage(0);
            break;

        default:
            return DefWindowProc(hwnd, msg, w_param, l_param);
    }

    return 0;
}

int WINAPI WinMain(HINSTANCE h_instance, HINSTANCE h_prev_instance,
                   LPSTR lp_cmd_line, int n_cmd_show)
{
    WNDCLASSEX wc;
    MSG msg;

    wc.cbSize = sizeof(WNDCLASSEX);
    wc.style = 0;
    wc.lpfnWndProc = WndProc;
    wc.cbClsExtra = 0;
    wc.cbWndExtra = 0;
    wc.hInstance = h_instance;
    wc.hIcon = LoadIcon(NULL, IDI_APPLICATION);
    wc.hCursor = LoadCursor(NULL, IDC_ARROW);
    wc.hbrBackground = (HBRUSH)(COLOR_WINDOW+1);
    wc.lpszMenuName = NULL;
    wc.lpszClassName = g_m_ClassName;
    wc.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

    if(!RegisterClassEx(&wc))
    {
        MessageBox(NULL, "Window registartion failed", "Error", MB_ICONEXCLAMATION | MB_OK);
        return 0;
    }

    HWND m_hwnd = CreateWindowEx(
            WS_EX_CLIENTEDGE,
            g_m_ClassName,
            "MAC Spoofer - Shady Looking Program Edition",
            WS_OVERLAPPEDWINDOW,
            CW_USEDEFAULT,
            CW_USEDEFAULT,
            400,
            100,
            NULL, NULL, h_instance, NULL
            );

    if(m_hwnd == NULL)
    {
        MessageBox(NULL, "Window creation failed", "Error", MB_ICONEXCLAMATION | MB_OK);
        return 0;
    }

    HWND hwnd_spoof_button = GetDlgItem(m_hwnd, IDC_BUTTON);

    if(hwnd_spoof_button == NULL)
    {
        MessageBox(NULL, "Spoof button creation failed", "Error", MB_ICONEXCLAMATION | MB_OK);
        return 0;
    }

    HWND hwnd_mac_edit = GetDlgItem(m_hwnd, IDC_EDIT);

    if(hwnd_mac_edit == NULL)
    {
        MessageBox(NULL, "Text edit creation failed", "Error", MB_ICONEXCLAMATION | MB_OK);
        return 0;
    }

    HWND hwnd_combobox = GetDlgItem(m_hwnd, IDC_COMBOBOX);

    if(hwnd_combobox == NULL)
    {
        MessageBox(NULL, "Combobox creation failed", "Error", MB_ICONEXCLAMATION | MB_OK);
        return 0;
    }

    PIP_ADAPTER_INFO p_adapter = Get_Adp_Info();
    int ret_code;

    while(p_adapter)
    {
        ret_code = SendMessage(hwnd_combobox, CB_ADDSTRING, 0, (LPARAM)p_adapter->Description);

        if (ret_code == CB_ERR)
        {
            MessageBox(NULL, "Error while populating combobox", "Error", MB_ICONEXCLAMATION | MB_OK);
        }
        else if (ret_code == CB_ERRSPACE)
        {
            MessageBox(NULL, "Error while populating combobox (not enough space)", "Error", MB_ICONEXCLAMATION | MB_OK);
        }

        p_adapter = p_adapter->Next;
    }

    SendMessage(hwnd_combobox, CB_SETCURSEL, (WPARAM) 0, (LPARAM) 0);

    ShowWindow(m_hwnd, n_cmd_show);
    UpdateWindow(m_hwnd);

    while(GetMessage(&msg, NULL, 0, 0) > 0)
    {
        TranslateMessage(&msg);
        DispatchMessage(&msg);
    }

    return msg.wParam;
}